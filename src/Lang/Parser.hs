{-# LANGUAGE NamedFieldPuns #-}

module Lang.Parser where

import Data.Set (Set)
import qualified Data.Set as Set
import Text.ParserCombinators.ReadP

import Lang.Lang

data Node =
    Leaf String
  | Branch [Node]
  deriving (Show, Eq)

typeToNode :: Type String -> Node
typeToNode ty =
  case ty of
    TyBool -> Leaf "Bool"
    TyNat -> Leaf "Nat"
    TyArr { inTy, outTy } -> Branch [typeToNode inTy, Leaf "->", typeToNode outTy]

termToNode :: Term String -> Node
termToNode tm =
  case tm of
    TmTrue -> Leaf "true"
    TmFalse -> Leaf "false"
    TmZero -> Leaf "zero"
    TmSucc { arg } -> Branch [Leaf "succ", termToNode arg]
    TmIsZero { arg } -> Branch [Leaf "isZero", termToNode arg]
    TmPred { arg } -> Branch [Leaf "pred", termToNode arg]
    TmIf { guard, thenCase, elseCase } -> Branch [Leaf "if", termToNode guard, termToNode thenCase, termToNode elseCase]
    TmVar { binding } -> Leaf binding
    TmApp { first, second } -> Branch [termToNode first, termToNode second]
    TmAbs { name, argTy, body } -> Branch [Leaf "lambda", Leaf name, typeToNode argTy, termToNode body]

reserved :: Set String
reserved = Set.fromList ["true", "false", "zero", "succ", "isZero", "pred", "if", "lambda", "Bool", "Nat"]

nodeToType :: Node -> Maybe (Type String)
nodeToType n =
  case n of
    Leaf s ->
      case s of
        "Bool" -> Just TyBool
        "Nat" -> Just TyNat
        _ -> Nothing
    Branch bs ->
      case bs of
        [b, Leaf "->", c] -> TyArr <$> nodeToType b <*> nodeToType c
        _ -> Nothing

nodeToTerm :: Node -> Maybe (Term String)
nodeToTerm n =
  case n of
    Leaf s ->
      case s of
        "true" -> Just TmTrue
        "false" -> Just TmFalse
        "zero" -> Just TmZero
        _ -> if Set.member s reserved then Nothing else Just $ TmVar s
    Branch bs ->
      case bs of
        [Leaf "lambda", Leaf ident, c, d] -> TmAbs ident <$> nodeToType c <*> nodeToTerm d
        [Leaf "succ", c] -> TmSucc <$> nodeToTerm c
        [Leaf "pred", c] -> TmPred <$> nodeToTerm c
        [Leaf "isZero", c] -> TmIsZero <$> nodeToTerm c
        [Leaf "if", c, d, e] -> TmIf <$> nodeToTerm c <*> nodeToTerm d <*> nodeToTerm e
        [b, c] -> TmApp <$> nodeToTerm b <*> nodeToTerm c
        _ -> Nothing

headOption :: [a] -> Maybe a
headOption [] = Nothing
headOption (x:_) = Just x

isEmpty :: String -> Bool
isEmpty [] = True
isEmpty (_:_) = False

runParser :: ReadP a -> String -> Maybe a
runParser rp s =
  let results = readP_to_S rp s
      full = flip filter results $ \(_, s) -> isEmpty s
      first = headOption full
    in fst <$> first

parseNode :: ReadP Node
parseNode = parseBranch <++ parseLeaf

parseBranch :: ReadP Node
parseBranch = do
  startParen
  children <- sepBy1 parseNode space
  endParen
  return $ Branch children

parseLeaf :: ReadP Node
parseLeaf = Leaf <$> munch1 (\c -> c /= ' ' && c /= '(' && c /= ')')

parseConstantString :: String -> x -> ReadP x
parseConstantString s x = fmap (const x) (string s)

parseConstantChar :: Char -> x -> ReadP x
parseConstantChar c x = fmap (const x) (char c)

startParen :: ReadP ()
startParen = parseConstantChar '(' ()

endParen :: ReadP ()
endParen = parseConstantChar ')' ()

space :: ReadP ()
space = parseConstantChar ' ' ()

{- | readNode
>>> readNode "zero"
Just (Leaf "zero")
>>> readNode "(foo)"
Just (Branch [Leaf "foo"])
>>> readNode "(succ zero)"
Just (Branch [Leaf "succ",Leaf "zero"])
-}
readNode :: String -> Maybe Node
readNode = runParser parseNode

{- | readType
>>> readType "Nat"
Just TyNat
-}
readType :: String -> Maybe (Type String)
readType s = flip runParser s $ do
    n <- parseNode
    maybe pfail return $ nodeToType n

{- | readTerm
>>> readTerm "zero"
Just TmZero
>>> readTerm "(succ zero)"
Just (TmSucc {arg = TmZero})
>>> readTerm "(succ x)"
Just (TmSucc {arg = TmVar {binding = "x"}})
>>> readTerm "(lambda x Nat (succ x))"
Just (TmAbs {name = "x", argTy = TyNat, body = TmSucc {arg = TmVar {binding = "x"}}})
-}
readTerm :: String -> Maybe (Term String)
readTerm s = flip runParser s $ do
  n <- parseNode
  maybe pfail return $ nodeToTerm n

{- | showNode
>>> showNode $ Leaf "zero"
"zero"
>>> showNode $ Branch [Leaf "succ", Leaf "zero"]
"(succ zero)"
-}
showNode :: Node -> String
showNode (Leaf value) = value
showNode (Branch children) =
  "(" ++ unwords (fmap showNode children) ++ ")"

{- | showType
>>> showType TyNat
"Nat"
>>> showType $ TyArr TyNat TyBool
"(Nat -> Bool)"
-}
showType :: Type String -> String
showType = showNode . typeToNode

{- | showTerm
>>> showTerm TmZero
"zero"
>>> showTerm (TmSucc {arg = TmZero})
"(succ zero)"
>>> showTerm (TmSucc {arg = TmVar {binding = "x"}})
"(succ x)"
>>> showTerm (TmAbs {name = "x", argTy = TyNat, body = TmSucc {arg = TmVar {binding = "x"}}})
"(lambda x Nat (succ x))"
-}
showTerm :: Term String -> String
showTerm = showNode . termToNode