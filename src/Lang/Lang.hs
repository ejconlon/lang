{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE Rank2Types #-}

module Lang.Lang where

import Data.Set (Set)
import qualified Data.Set as Set

data Term b =
    TmTrue
  | TmFalse
  | TmIf { guard :: Term b, thenCase :: Term b, elseCase :: Term b }
  | TmZero
  | TmSucc { arg :: Term b }
  | TmPred { arg :: Term b }
  | TmIsZero { arg :: Term b }
  | TmVar { binding :: b }
  | TmApp { first :: Term b, second :: Term b }
  | TmAbs { name :: String, argTy :: Type b, body :: Term b }
  deriving (Show, Eq)

data Type b =
    TyNat
  | TyBool
  | TyArr { inTy :: Type b, outTy :: Type b }
  deriving (Show, Eq)

data TypeError b =
    UnexpectedError { expected :: Type b, term :: Term b, actual :: Type b }
  | MismatchError { left :: Type b, right :: Type b }
  | NotFunction { left :: Type b, right :: Type b }
  | MissingBinding { missingBinding :: b }
  deriving (Show, Eq)

freeVars :: Term String -> Set String
freeVars tm =
  case tm of
    TmTrue -> Set.empty
    TmFalse -> Set.empty
    TmIf { guard, thenCase, elseCase } -> freeVars guard `Set.union` freeVars thenCase `Set.union` freeVars elseCase
    TmZero -> Set.empty
    TmSucc { arg } -> freeVars arg
    TmPred { arg } -> freeVars arg
    TmIsZero { arg } -> freeVars arg
    TmVar { binding } -> Set.singleton binding
    TmApp { first, second } -> freeVars first `Set.union` freeVars second
    TmAbs { name, body } -> Set.delete name $ freeVars body

namedToNameless :: Context () -> Term String -> Maybe (Term Int)
namedToNameless ctx tm =
  case tm of
    TmTrue -> Just TmTrue
    TmFalse -> Just TmFalse
    TmIf { guard, thenCase, elseCase } -> do
      guard2 <- namedToNameless ctx guard
      thenCase2 <- namedToNameless ctx thenCase
      elseCase2 <- namedToNameless ctx elseCase
      return TmIf { guard = guard2, thenCase = thenCase2, elseCase = elseCase2 }
    TmZero -> Just TmZero
    TmSucc { arg } -> (\x -> TmSucc { arg = x }) <$> namedToNameless ctx arg
    TmPred { arg } -> (\x -> TmPred { arg = x }) <$> namedToNameless ctx arg
    TmIsZero { arg } -> (\x -> TmIsZero { arg = x }) <$> namedToNameless ctx arg
    TmVar { binding } -> (\x -> TmVar { binding = x }) <$> indexOfName ctx binding
    TmApp { first, second } -> TmApp <$> namedToNameless ctx first <*> namedToNameless ctx second
    TmAbs { name, body } ->
      let ctx2 = updateLookup ctx name ()
        in namedToNameless ctx2 body

namelessToNamed :: Context () -> Term Int -> Maybe (Term String)
namelessToNamed = undefined

substitute :: Term Int -> Int -> Term Int -> Term Int
substitute value index tm =
  case tm of
    TmTrue -> TmTrue
    TmFalse -> TmFalse
    TmIf { guard, thenCase, elseCase } ->
      TmIf {
        guard = substitute value index guard,
        thenCase = substitute value index thenCase,
        elseCase = substitute value index elseCase
      }
    TmZero -> TmZero
    TmSucc { arg } -> TmSucc { arg = substitute value index arg }
    TmPred { arg } -> TmPred { arg = substitute value index arg }
    TmIsZero { arg } -> TmIsZero { arg = substitute value index arg }
    TmVar { binding } -> if binding == index then value else tm
    TmApp { first, second } ->
      TmApp {
        first = substitute value index first,
        second = substitute value index second
      }
    TmAbs { name, argTy, body } ->
      let newValue = shift 1 0 value in
        TmAbs {
          name = name,
          argTy = argTy,
          body = substitute newValue index body
        }

shift :: Int -> Int -> Term Int -> Term Int
shift places cutoff tm =
  case tm of
    TmTrue -> TmTrue
    TmFalse -> TmFalse
    TmIf { guard, thenCase, elseCase } ->
      TmIf {
        guard = shift places cutoff guard,
        thenCase = shift places cutoff thenCase,
        elseCase = shift places cutoff elseCase
      }
    TmZero -> TmZero
    TmSucc { arg } -> TmSucc { arg = shift places cutoff arg }
    TmPred { arg } -> TmPred { arg = shift places cutoff arg }
    TmIsZero { arg } -> TmIsZero { arg = shift places cutoff arg }
    TmVar { binding } ->
      let newBinding = if binding < cutoff then binding else binding + places
        in TmVar { binding = newBinding }
    TmApp { first, second } ->
      TmApp {
        first = shift places cutoff first,
        second = shift places cutoff second
      }
    TmAbs { name, argTy, body } ->
      TmAbs {
        name = name,
        argTy = argTy,
        body = shift places (cutoff + 1) body
      }

newtype Context x = Context [(String, x)]

type Lookup b = forall x. Context x -> b -> Maybe x

lookupByName :: Lookup String
lookupByName (Context c) s = lookup s c

lookupByIndex :: Lookup Int
lookupByIndex (Context c) i = snd <$> safeIndex c i

indexOfName :: Context x -> String -> Maybe Int
indexOfName (Context cs) = go 0 cs
  where
    go :: Int -> [(String, x)] -> String -> Maybe Int
    go _ [] _ = Nothing
    go i ((a, _):as) s | a == s = Just i
                       | otherwise = go (i + 1) as s

updateLookup :: Context x -> String -> x -> Context x
updateLookup (Context c) n x = Context $ (n, x):c

safeIndex :: [a] -> Int -> Maybe a
safeIndex _ i | i < 0 = Nothing
safeIndex [] _ = Nothing
safeIndex (x:xs) i | i == 0 = Just x
                   | otherwise = safeIndex xs (i - 1)

expectType :: Lookup b -> Context (Type b) -> Term b -> Type b -> Either (TypeError b) (Type b)
expectType look ctx tm ty =
  case typeOf look ctx tm of
    Right otherTy ->
      if ty == otherTy then Right ty
      else Left UnexpectedError { expected = ty, term = tm, actual = otherTy }
    Left err -> Left err

typeOf :: Lookup b -> Context (Type b) -> Term b -> Either (TypeError b) (Type b)
typeOf look ctx tm =
  case tm of
    TmTrue -> Right TyBool
    TmFalse -> Right TyBool
    TmZero -> Right TyNat
    TmSucc { arg } -> expectType look ctx arg TyNat
    TmPred { arg } -> expectType look ctx arg TyNat
    TmIsZero { arg } -> expectType look ctx arg TyNat
    TmIf { guard, thenCase, elseCase } -> do
      _ <- expectType look ctx guard TyBool
      tTy <- typeOf look ctx thenCase
      eTy <- typeOf look ctx elseCase
      if tTy == eTy then Right tTy else Left MismatchError { left = tTy, right = eTy }
    TmVar { binding } ->
      let err = Left $ MissingBinding binding
          bound = look ctx binding
        in maybe err Right bound
    TmApp { first, second } -> do
      fTy <- typeOf look ctx first
      sTy <- typeOf look ctx second
      case fTy of
        TyArr { inTy, outTy } | inTy == sTy -> Right outTy
        _ -> Left NotFunction { left = fTy, right = sTy }
    TmAbs { name, argTy, body } ->
      let ctx2 = updateLookup ctx name argTy
        in do
          outTy <- typeOf look ctx2 body
          return TyArr { inTy = argTy, outTy = outTy }

isValue :: Term b -> Bool
isValue tm =
  case tm of
    TmTrue -> True
    TmFalse -> True
    TmZero -> True
    TmSucc { arg } -> isValue arg
    TmPred { arg } -> isValue arg
    TmIsZero { arg } -> isValue arg
    TmVar {} -> True
    TmAbs {} -> True
    _ -> False

smallStep :: Context (Term Int) -> Term Int -> Maybe (Term Int)
smallStep ctx tm =
  case tm of
    TmSucc { arg }
      | not (isValue arg) -> do
        x <- smallStep ctx arg
        return $ TmSucc x
    TmPred { arg }
      | not (isValue arg) -> do
        x <- smallStep ctx arg
        return $ TmPred x
      | otherwise ->
        case arg of
          TmZero -> Just TmZero
          TmSucc { arg } -> Just arg
          _ -> Nothing
    TmIsZero { arg }
      | not (isValue arg) -> do
        x <- smallStep ctx arg
        return $ TmIsZero x
      | otherwise ->
        case arg of
          TmZero -> Just TmTrue
          TmSucc {} -> Just TmFalse
          _ -> Nothing
    ifTm@(TmIf { guard, thenCase, elseCase })
      | not (isValue guard) -> do
        x <- smallStep ctx guard
        return $ ifTm { guard = x }
      | not (isValue thenCase) -> do
        x <- smallStep ctx thenCase
        return $ ifTm { thenCase = x }
      | not (isValue elseCase) -> do
        x <- smallStep ctx elseCase
        return $ ifTm { elseCase = x }
      | otherwise ->
        case guard of
          TmTrue {} -> Just thenCase
          TmFalse {} -> Just elseCase
          _ -> Nothing
    TmVar { binding } -> lookupByIndex ctx binding
    TmApp { first, second}
      | not (isValue first) -> do
        x <- smallStep ctx first
        return TmApp { first = x, second = second }
      | not (isValue second) -> do
        x <- smallStep ctx second
        return TmApp { first = first, second = x }
      | otherwise ->
        case first of
          TmAbs { body } ->
            let value = shift 1 0 second
                subbed = substitute value 0 body
                shifted = shift (-1) 0 subbed
              in Just shifted
          _ -> Nothing
    _ -> Nothing

bigStep :: Context (Term Int) -> Term Int -> Term Int
bigStep ctx tm =
  case smallStep ctx tm of
    Just next -> bigStep ctx next
    Nothing -> tm
