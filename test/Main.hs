module Main where

import Test.DocTest
import Test.HUnit.Base
import Test.HUnit.Text

testEmpty = TestCase $ assertEqual
  "addition should work"
  2
  2

main = do
  doctest ["src"]
  runTestTT testEmpty
